var scope = {
       container: document.querySelector('[data-module="app"]'),
init(){
    this.container.innerHTML = this.template;    
    scope.render();
},
render(){
    var allData = document.querySelectorAll('[data-value]');
    allData.forEach(element => {
        if(element.getAttribute("data-value") == 'seconds')
          element.innerHTML = this.data.seconds;
          if(element.getAttribute("data-value") == 'minutes')
          element.innerHTML = this.data.minutes;
    });
   this.mount();
},
mount(){
    document.querySelector('[data-value="seconds"]').innerHTML = this.data.seconds;
    document.querySelector('[data-value="minutes"]').innerHTML = this.data.minutes;
},
    template: 
    `<article class="reloj">
    <section class="display">
        <span class="seconds" data-value="seconds"></span>
        <span class="minutes" data-value="minutes"></span>
    </section>
    <section class="controls">
            <button>Start</button>
            <button>Pause</button>
            <button>Stop</button>
    </section>
    </article>`,
    data:{seconds:0, minutes:0}
    
}
scope.init();
setInterval(function() {
    
    scope.data.seconds++;
    scope.data.minutes++;   
    
    scope.render();  
  }, 1000)
